Source: ppx-fields-conv
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Stéphane Glondu <glondu@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ocaml-nox,
 ocaml-dune,
 libbase-ocaml-dev (>= 0.14),
 libfieldslib-ocaml-dev (>= 1:0.14),
 libppxlib-ocaml-dev (>= 0.14.0),
 dh-ocaml
Standards-Version: 4.5.0
Rules-Requires-Root: no
Section: ocaml
Homepage: https://github.com/janestreet/ppx_fields_conv
Vcs-Git: https://salsa.debian.org/ocaml-team/ppx-fields-conv.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ppx-fields-conv

Package: libppx-fields-conv-ocaml-dev
Architecture: any
Depends:
 libfieldslib-ocaml-dev,
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: generation of accessor and iteration functions for OCaml records (dev)
 ppx_fields_conv is a ppx rewriter that can be used to define first
 class values representing record fields, and additional routines, to
 get and set record fields, iterate and fold over all fields of a
 record and create new record values.
 .
 This package contains development files.

Package: libppx-fields-conv-ocaml
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Description: generation of accessor and iteration functions for OCaml records (runtime)
 ppx_fields_conv is a ppx rewriter that can be used to define first
 class values representing record fields, and additional routines, to
 get and set record fields, iterate and fold over all fields of a
 record and create new record values.
 .
 This package contains runtime files.
